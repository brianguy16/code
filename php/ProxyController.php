<?php
namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use App\Models\Sites\Site;
use App\Models\Sites\Type;
use Illuminate\Exception;
use App\Http\Controllers\Api\V1\Admin\AdminController;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use GuzzleHttp\TransferStats;


class ProxyController extends AdminController
{
    const FALLBACK_URL_FOR_IFRAME_ERRORS = 'https://s3.amazonaws.com/editor-theme.pixelpop.co/index.html';

    public function getProxySite(Request $request)
    {
        $iframe_enabled = env('PP_ENABLE_PROXY_FEATURE', false);

        if (!$iframe_enabled) {
            $message = [
                'action' => 'proxy',
                'status' => 'disabled'
            ];

            return view('fakepage', ['state' => json_encode($message) ]);
        }

        $storefrontPassword = null;

        if ($request->has('access')) {
            $storefrontPassword = $request->query('access');
        }

        try
        {
            if (!$this->site) {
                $message = [
                    'action' => 'proxy',
                    'status' => 'noauth'
                ];

                return view('fakepage', ['state' => json_encode($message) ]);
            }

            $client = new Client();
            $response = $client->request('GET', 'https://' . $this->site->domain,
                ['allow_redirects' => false],
                ['debug' => false],
                ['connect_timeout' => 10],
                [
                    'headers' => [
                        'User-Agent' => 'proxy/proxy',
                        'Accept'     => 'text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8'
                    ]
                ]
            );

        } catch (\Exception $e) {
            $message = [
                'action' => 'proxy',
                'status' => 'exception'
            ];

            return view('fakepage', ['state' => json_encode($message)]);
        }

        if ($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();

        } else {
            if ($response->getStatusCode() === 302 && !empty($storefrontPassword)) {
                $protectedPage = $this->getProtectedPage($response->getHeader('Location')[0], $storefrontPassword);

                return $protectedPage;
            }

            $message = [
                'action' => 'proxy',
                'status' => $response->getStatusCode()
            ];

            return view('fakepage', ['state' => json_encode($message) ]);
        }
    }

    protected function getProtectedPage ($passwordUrl, $password) {
        $client = new Client(['cookies' => true]);
        $postResponse = $client->request('POST', $passwordUrl, [
            'multipart' => [
                [
                    'name' => 'password',
                    'contents' => $password
                ]
                ],
                'on_stats' => function (TransferStats $stats) use (&$responseUrl) {
                    $responseUrl = $stats->getEffectiveUri();
                }
        ]);

        if ($passwordUrl == $responseUrl) {
            $message = [
                'action' => 'proxy',
                'status' => 'invalid'
            ];

            return view('fakepage', ['state' => json_encode($message) ]);

        } else {
            return $postResponse->getBody()->getContents();
        }
    }
}
