<?php
namespace App\Http\Controllers\Api\V1\Admin\Site\Markdown;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Parsedown;

class MarkdownController extends Controller
{
    public function html(Request $request)
    {
        $this->validate($request, [
            'bodyText' => 'required',
        ]);
        $bodyText = $request->input('bodyText');
        $parsedown = new Parsedown();
        $bodyTextHtml = $parsedown
                        ->setBreaksEnabled(true)
                        ->setMarkupEscaped(true)
                        ->line($bodyText);

        return response()->json(['data'=>['bodyTextHtml' => $bodyTextHtml]]);
    }
}
