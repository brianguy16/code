<?php
namespace App\Libraries\Klaviyo;

use GuzzleHttp\Exception\BadResponseException;

class KlaviyoApi
{
    const DEFAULT_BASE_API_URL = 'https://a.klaviyo.com';
    const API_SINGLE_LIST_ROUTE = '/api/v1/list';
    const API_LISTS_ROUTE = '/api/v1/lists';
    const LIST_SOURCE = "Pixelpop";

    private $guzzle;
    private $apiListUrl;
    private $apiListsUrl;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiListsUrl = env('KLAVIYO_BASE_URL', self::DEFAULT_BASE_API_URL) . self::API_LISTS_ROUTE;
        $this->apiListUrl = env('KLAVIYO_BASE_URL', self::DEFAULT_BASE_API_URL) . self::API_SINGLE_LIST_ROUTE;
        $this->apiKey = $apiKey;
        $this->guzzle = new \GuzzleHttp\Client();
    }

    public function getLists()
    {
        $query = [
            'api_key' => $this->apiKey,
            'type' => 'list'
        ];

        try {
            $response = $this->guzzle->get($this->apiListsUrl, ['query' => $query]);

            $statusCode = $response->getStatusCode();
            $lists = json_decode($response->getBody())->data;

            return $this->createResponse($statusCode, $lists);
        } catch (BadResponseException $ce) {
            $response = json_decode($ce->getResponse()->getBody());

            return $this->createResponse($response->status, $response->message);
        }
    }

    public function apiKeyIsValid()
    {
        $query = ['api_key' => $this->apiKey];

        try {
            $response = $this->guzzle->get($this->apiListsUrl, ['query' => $query]);

            return $response->getStatusCode() === 200;
        } catch (BadResponseException $ce) {
            return false;
        }

        return false;
    }

    private function createResponse($statusCode, $content)
    {
        return [
            'status' => $statusCode,
            'response' => $content
        ];
    }

    public function assignEmailToList($emailAddress, $listId, $confirmOptin = true)
    {
        try {
            $response = $this->guzzle->request('POST', $this->apiListUrl . '/' . $listId . '/members', [
                'form_params' => [
                    'api_key' => $this->apiKey,
                    'email' => $emailAddress,
                    'confirm_optin' => $confirmOptin
                ],
            ]);

            return [
                'success' => true
            ];
        } catch (BadResponseException $ce) {
            $response = json_decode($ce->getResponse()->getBody());

            return [
                'success' => false,
                'body' => $response
            ];
        }
    }
}
