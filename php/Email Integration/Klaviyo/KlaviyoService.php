<?php
namespace App\Libraries\Klaviyo;

use App\Libraries\Klaviyo\KlaviyoApi;
use App\Libraries\Utility\MailIntegrationProviderTypeEnumeration;
use App\Models\Sites\IntegrationCredential;
use App\Models\Sites\Modules\Instance_mail_sync;
use App\Models\Sites\Site;

class KlaviyoService
{
    private $api;
    private $integrationCredential;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->api = new KlaviyoApi($this->apiKey);
    }

    public function connectSite(Site $site)
    {
        if ($this->api->apiKeyIsvalid()) {
            $this->integrationCredential = new IntegrationCredential(
                [
                    'auth_key' => $this->apiKey,
                    'provider' => MailIntegrationProviderTypeEnumeration::KLAVIYO
                ]
            );
            
            return $site->integrationCredentials()->save($this->integrationCredential);
        }

        return false;
    }

    public function disconnectSite(Site $site)
    {
        $deletedRows = $site->integrationCredentials()->where('auth_key', $this->apiKey)->delete();

        return $deletedRows > 0;
    }

    public function subscribe($emailAddress, $listId)
    {
        return $this->api->assignEmailToList($emailAddress, $listId);
    }

    public function assignListToInstance($listId, $instanceId)
    {
        if ($list = $this->getListById($listId)) {
            return $this->createMailSync($list, $instanceId);
        }
    }

    public function unassignListFromInstance($instanceId)
    {
        $deletedRows = Instance_mail_sync::where('instance_id', $instanceId)->delete();

        return $deletedRows > 0;
    }

    public function getLists()
    {
        return $this->api->getLists();
    }

    private function getListById($listId)
    {
        return collect($this->getLists()['response'])->where('id', $listId)->first();
    }

    private function createMailSync($list, $instanceId)
    {
        $integrationCredential = IntegrationCredential::where('auth_key', $this->apiKey)
            ->where('provider', MailIntegrationProviderTypeEnumeration::KLAVIYO)
            ->first();
        $sync = Instance_mail_sync::firstOrNew(['instance_id' => $instanceId]);
        $sync->list = $list;
        $sync->provider_type_id = MailIntegrationProviderTypeEnumeration::KLAVIYO;
        $sync->sync_enabled = true;
        
        return $integrationCredential->mailSyncs()->save($sync);
    }
}
