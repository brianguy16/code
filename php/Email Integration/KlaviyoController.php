<?php
namespace App\Http\Controllers\Api\V1\Admin\Site;

use App\Http\Controllers\Api\V1\Admin\AdminController;
use App\Libraries\Utility\MailIntegrationProviderTypeEnumeration;
use Dingo\Api\Http\Request;
use App\Libraries\Klaviyo\KlaviyoService;
use App\Models\Sites\IntegrationCredential as Integration;

class KlaviyoController extends AdminController
{
    //TODO: add logic to these methods to call the appropriate Klaviyo code
    public function index(Request $request)
    {
        abort('400', 'Invalid');
    }

    public function create(Request $request)
    {
        $this->validate($request, ['apiKey' => 'required']);
        $klaviyo = new KlaviyoService($request->input('apiKey'));
        
        return response()->json(['data' => ['success' => $klaviyo->connectSite($this->site)]]);
    }

    public function update(Request $request)
    {
        abort('400', 'Invalid');
    }

    public function delete(Request $request)
    {
        $apiKey = $this->site
            ->integrationCredentials()
            ->where('provider', MailIntegrationProviderTypeEnumeration::KLAVIYO)->first()->auth_key;

        $klaviyo = new KlaviyoService($apiKey);
        return response()->json(['data' => ['success' => $klaviyo->disconnectSite($this->site)]]);
    }
}
