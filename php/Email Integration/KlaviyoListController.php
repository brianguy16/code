<?php
namespace App\Http\Controllers\Api\V1\Admin\Site;

use App\Http\Controllers\Api\V1\Admin\AdminController;
use App\Libraries\Klaviyo\KlaviyoService;
use App\Libraries\Utility\MailIntegrationProviderTypeEnumeration;
use App\Models\Sites\IntegrationCredential as Integration;
use Dingo\Api\Http\Request;

class KlaviyoListController extends AdminController
{
    public function index(Request $request)
    {
        $klaviyo = new KlaviyoService($this->getSiteApikey());
        
        return response()->json(['data' => $klaviyo->getLists()]);
    }

    public function create(Request $request, $instanceId)
    {
        $this->validate($request, ['listId' => 'required']);
        $klaviyo = new KlaviyoService($this->getSiteApikey());

        return response()->json(['data' => $klaviyo->assignListToInstance($request->input('listId'), $instanceId)]);
    }

    public function delete(Request $request, $instanceId)
    {
        $klaviyo = new KlaviyoService($this->getSiteApikey());

        if ($klaviyo->unassignListFromInstance($instanceId)) {
            return response()->json(['message' => 'list deleted'], 200);
        }

        return response()->json(['message' => 'delete failed'], 400);
    }
    
    private function getSiteApiKey()
    {
        return $this->site
            ->integrationCredentials()
            ->where('provider', MailIntegrationProviderTypeEnumeration::KLAVIYO)
            ->first()
            ->auth_key;
    }
}
