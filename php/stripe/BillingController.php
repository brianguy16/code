<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Libraries\Stripe\StripePlanChangeHandler;
use App\Libraries\Stripe\StripeUtilities;
use Dompdf\Dompdf;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class BillingController extends AdminController
{
    const GENERIC_STRIPE_ERROR_MESSAGE = 'Oops, something went wrong. Try reloading the page and contact us if the problem persists.';

    public function addCardSubscription(Request $request)
    {
        $this->validate($request, [
            'stripeToken.id' => 'required',
            'planName' => 'required',
        ]);

        try {
            $customerName = $request->input('stripeToken.card.name');
            $stripeToken = $request->input('stripeToken.id');
            $planName = $request->input('planName');

            $response = StripeUtilities::i($this->site)->createSubscription($planName, $stripeToken, $customerName);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['data'=>['error' => $this->getErrorMessage($e)]]);
        }

        return response()->json(['data'=>['plan_friendly_name' => $response->friendly_name]]);
    }

    public function updateCard(Request $request)
    {
        $this->validate($request, [
            'stripeToken.id' => 'required'
        ]);

        try {
            $customerName = $request->input('stripeToken.card.name');
            $stripeToken = $request->input('stripeToken.id');
            StripeUtilities::i($this->site)->updateCard($stripeToken);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['data'=>['success' => false, 'error' => $this->getErrorMessage($e)]]);
        }

        return response()->json(['data'=>['success' => true, 'error' => null]]);
    }

    public function changeSubscription(Request $request)
    {
        $this->validate($request, [
            'newPlan' => 'required',
        ]);

        try {
            $newPlan = $request->input('newPlan');
            $plan = StripeUtilities::i($this->site)->changeSubscription($newPlan);

            $handler = new StripePlanChangeHandler();
            $response = $handler->switchPlan($this->site, $plan);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['data'=>['success' => false, 'error' => self::GENERIC_STRIPE_ERROR_MESSAGE]]);
        }

        return response()->json([
            'data' => [
                'success' => !is_null($response) || ($newPlan === 'free' && is_null($response)),
                'newPlan' => $newPlan,
                'error' => null
            ]
        ]);
    }

    public function getInvoices()
    {
        $invoices = [];

        try {
            $invoices = StripeUtilities::i($this->site)->getInvoices();
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['data'=>['error' => self::GENERIC_STRIPE_ERROR_MESSAGE]]);
        }

        return response()->json(['data' => ['invoices' => $invoices]]);
    }

    public function downloadInvoice(Request $request, $invoiceId)
    {
        try {
            $invoice = StripeUtilities::i($this->site)->getInvoiceAsHtml($invoiceId);

            $dompdf = new Dompdf();
            $dompdf->loadHtml($invoice);
            $dompdf->render();

            $filename = 'Pixelpop-' . $invoiceId . '-' . Carbon::now()->timestamp;

            $dompdf->stream($filename);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->view('errors.400');
        }
    }

    private function getErrorMessage(\Exception $e)
    {
        $type = get_class($e);

        switch ($type) {
            case 'Stripe_CardError':
                return $e->getMessage();
            default:
                return self::GENERIC_STRIPE_ERROR_MESSAGE;
        }
    }
}