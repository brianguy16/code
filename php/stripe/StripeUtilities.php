<?php
namespace App\Libraries\Stripe;

use App\Libraries\Utility\PlansEnumeration;
use App\Models\Plan;
use App\Models\Sites\Site;
use App\Models\Sites\Stripe_billing;
use App\Libraries\Taxes\SiteTaxRateHelper;
use Illuminate\Support\Facades\Log;

class StripeUtilities
{
    private $site;

    private const PRODUCT = 'Product';
    private const VENDOR = 'Vendor';
    private const STREET = 'Street';
    private const LOCATION = 'Location';
    private const EMAIL = 'abc123@email.com';

    protected function __construct(Site $site)
    {
        $this->site = $site;
    }

    public static function i(Site $site) {
        return new StripeUtilities($site);
    }

    public function createSubscription($planName, $stripeToken, $customerName)
    {
        $taxPercent = SiteTaxRateHelper::i($this->site)->getTaxPercentBySite();
        $plan = Plan::where('internal_name', $planName)->firstOrFail();
        $stripeBilling = Stripe_billing::firstOrCreate(['site_id' => $this->site->id]);
        $this->setStripeSecret($stripeBilling);
        $stripeBilling->subscription($plan->internal_name)
                      ->withTax($taxPercent)
                      ->create($stripeToken, ['description' => $customerName]);

        return $plan;
    }

    public function updateCard($stripeToken)
    {
        $stripeBilling = Stripe_billing::where(['site_id' => $this->site->id])->firstOrFail();
        $this->setStripeSecret($stripeBilling);
        $stripeBilling->updateCard($stripeToken);
    }

    public function changeSubscription($newPlan)
    {
        $taxPercent = SiteTaxRateHelper::i($this->site)->getTaxPercentBySite();
        $plan = Plan::where('internal_name', $newPlan)->firstOrFail();
        $siteBilling = Stripe_billing::where(['site_id' => $this->site->id])->firstOrFail();
        $this->setStripeSecret($siteBilling);
        $siteBilling->subscription($plan->internal_name)
                    ->withTax($taxPercent)
                    ->prorate()
                    ->swap();
        $siteBilling->stripe_active = $plan->internal_name !== PlansEnumeration::FREE_PLAN_INTERNAL_NAME;
        $siteBilling->save();

        return $plan;
    }

    public function getInvoices()
    {
        $invoices = [];
        $stripeBilling = Stripe_billing::where(['site_id' => $this->site->id])->firstOrFail();
        $this->setStripeSecret($stripeBilling);

        if ($stripeBilling->hasStripeId()) {
            $stripeInvoices = collect($stripeBilling->invoices());
        } else {
            $stripeInvoices = [];
        }

        foreach($stripeInvoices as $invoice) {
            $invoices[] = [
                'id' => $invoice->id,
                'date' => $invoice->dateString(),
                'total' => $invoice->total()
            ];
        }

        return $invoices;
    }

    public function getInvoiceAsHtml($invoiceId)
    {
        $stripeBilling = Stripe_billing::where(['site_id' => $this->site->id])->firstOrFail();
        $this->setStripeSecret($stripeBilling);
        $taxAmount = SiteTaxRateHelper::i($this->site)->getTaxAmountBySite();

        $invoice = $stripeBilling->findInvoice($invoiceId)->render([
            'product' => self::PRODUCT,
            'vendor' => self::VENDOR,
            'street' => self::STREET,
            'location' => self::LOCATION,
            'email' => self::EMAIL,
            'taxAmount' => $taxAmount
        ]);

        return $invoice;
    }

    public function cancelSubscription()
    {
        $stripeBilling = Stripe_billing::where(['site_id' => $this->site->id])->first();

        if ($stripeBilling && $stripeBilling->stripeIsActive()) {
            try {
                $this->setStripeSecret($stripeBilling);
                $stripeBilling->subscription(PlansEnumeration::FREE_PLAN_INTERNAL_NAME)
                              ->prorate()
                              ->swap();
                $stripeBilling->stripe_active = false;
                $stripeBilling->save();
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    public function getStripeSecret()
    {
        return $this->site->test_mode === 1 || env('APP_ENV') !== 'production'
            ? env('STRIPE_TEST_SECRET')
            : env('STRIPE_SECRET');
    }

    private function setStripeSecret(Stripe_billing &$billing)
    {
        $key = $this->getStripeSecret();

        $billing::setStripeKey($key);
    }
}
