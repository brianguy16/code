(function() {
  'use strict';

  angular.module('app.controllers').controller('TargetDeviceBuilderDialogController', TargetDeviceBuilder);

  function TargetDeviceBuilder(DialogService, $scope, TargetDeviceService) {
    var vm = this;
    vm.devices = TargetDeviceService.getDevices();
    vm.targetDevice = $scope.device ? $scope.device : '';

    vm.abort = function() {
      DialogService.hide('cancel');
    };

    vm.save = function() {
      DialogService.hide(vm.targetDevice);
    };
  }
})();
