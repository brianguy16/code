(function() {
  'use strict';

  angular.module('app.directives').directive('billingCardInfo', billingCardInfoComponent);

  function billingCardInfoComponent() {
    return {
      restrict: 'E',
      templateUrl: './views/directives/billingCardInfo/billingCardInfo.component.html',
      controller: BillingCardInfoController,
      controllerAs: 'vm',
      scope: {
        action: '@',
        planname:'=',
        site: '='
      }
    };
  }


  var BillingCardInfoController = function($scope, $rootScope, API, Environment) {
    var vm = this;
    //TODO: handle what would happen if this api key is missing
    var stripe = Stripe(getStripeKey());
    vm.stripeError = null;
    $scope.addingCard = false;
    $scope.updatingCard = false;
    var elements = stripe.elements();

    var style = {
      base: {
        iconColor: '#C0C0C0',
        iconStyle: 'solid',
        color: '#2f2b33',
        lineHeight: '1.43',
        padding: '8px',
        fontFamily: '"Circular", sans-serif',
        fontSize: '14px',
        '::placeholder': {
          color: '#aaa8ac',
        }
      },
      invalid: {
        color: '#f45540',
        iconColor: '#f45540'
      }
    };

    //create an instance of the card Element into 'card-element'
    var card = elements.create('card', { style: style });
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      if (event.error) {
        handleCardErrors(event);
      } else {
        vm.stripeError = false;
      }
    });
    
    $scope.$on('creditCard.add', function() {
      addUpdateCreditCard();
    });

    vm.updateCard = function() {
      if ($scope.updatingCard == false) {
        addUpdateCreditCard();
      }
    };
  
    vm.hideCard = function() {
      $rootScope.$broadcast('updateCard.hide');
    };
  
    function handleCardErrors(event) {
      vm.stripeError = event ? event.error.message : null;
      $rootScope.$broadcast('creditCard.error');
    }

    function addUpdateCreditCard() {
      vm.stripeError = null;
      vm.submitted = true;
      if ($scope.cardForm.cardholderName.$valid) {
        stripe.createToken(card, {name: $scope.cardholderName}).then(function(result) {
          if (result.error) {
            handleCardErrors(result);
          } else {
            vm.submitted = false;
            handleStripeToken(result.token);
          }
        });
      } else {
        handleCardErrors(null);
      }
    }

    function handleStripeToken(token) {
      var data = {
          stripeToken: token
      };

      if ($scope.action === 'add') {
        data.planName = $scope.planname;
        addCreditCard(data);
      } else {
        updateCreditCard(data);
      }
    }

    function addCreditCard(data) {
      $scope.addingCard = true;
      $scope.stripeErrors = null;
      API.one('card').post(null, data).then(function(response) {
        if (response.plan_friendly_name) {
          $rootScope.$broadcast('creditCard.added', response.plan_friendly_name);
        } else {
          if (response.error) {
            vm.stripeError = response.error;
          } else {
            vm.stripeError = 'Something went wrong';
          }
        }
        $rootScope.$broadcast('creditCard.error');
      });
    }

    function updateCreditCard(data) {
      $scope.updatingCard = true;
      $scope.stripeErrors = null;
      API.one('card').customPUT(data).then(function(response) {
        if (response.success) {
          $rootScope.$broadcast('creditCard.updated');
        } else {
          if (response.error) {
            vm.stripeError = response.error;
          } else {
            vm.stripeError = 'Something went wrong';
          }
        }
        $scope.updatingCard = false;
      });
    }
    
    function getStripeKey() {
      return Environment.environment !== 'production' || $scope.site.test_mode === 1
        ? Environment.stripeApiTestKey
        : Environment.stripeApiKey;
    }
  };
})();
