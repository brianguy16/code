(function() {
  'use strict';

  angular.module('app.controllers').controller('AddCardDialogController', AddCard);

  function AddCard(DialogService, $scope, $rootScope) {
    var vm = this;
    
    vm.planName = $scope.requested_plan_name;
    
    $rootScope.$on('creditCard.added', function() {
      DialogService.hide($scope);  //what data to return here...
    });
  
    $rootScope.$on('creditCard.error', function() {
      $scope.addingCard = false;
    });

    vm.abort = function() {
      DialogService.hide(null);
    };

    vm.add = function() {
      $scope.addingCard = true;
      $scope.$broadcast('creditCard.add');
    };
  }
})();