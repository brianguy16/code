(function() {
  "use strict";

  angular.module('app.directives').directive('billingInvoices', billingInvoices);

  function billingInvoices() {

    return {
      restrict: 'E',
      templateUrl: './views/directives/billingInvoices/billingInvoices.component.html',          
      controller: BillingInvoicesController,
      controllerAs: 'vm',
      scope: false,
      bindToController: true
    };
  }

  function BillingInvoicesController($scope, $rootScope, $window, $auth, API) {
    var vm = this;
    vm.invoicesLoaded = false;

    API.one('invoices').get().then(function(response) {
      vm.invoices = response.invoices;
      vm.invoicesLoaded = true;
    });        
  
    vm.downloadInvoiceHTML = function(invoiceId) {      
      $window.open('api/invoice/' + invoiceId + '?token=' + $auth.getToken());
    };

    vm.hideInvoices = function() {
      $rootScope.$broadcast('invoices.hide'); 
    };
  }
})();
