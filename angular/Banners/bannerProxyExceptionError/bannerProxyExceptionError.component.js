(function() {
  'use strict';

  angular.module('app.directives').directive('bannerProxyExceptionError', bannerProxyExceptionError);

  function bannerProxyExceptionError() {

    return {
      restrict: 'E',
      templateUrl: './views/directives/bannerProxyExceptionError/bannerProxyExceptionError.component.html',
      controller: function() {
        var vm = this;
        vm.showBanner = true;
        vm.dismiss = function() {
          vm.showBanner = false;
        };
      },
      controllerAs: 'vm',
      scope: false,
      replace: true,
      bindToController: true
    };
  }
})();
