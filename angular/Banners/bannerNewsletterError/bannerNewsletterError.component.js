(function() {
  "use strict";

  angular
    .module("app.directives")
    .directive("bannerNewsletterError", bannerNewsletterErrorComponent);

  function bannerNewsletterErrorComponent() {
    return {
      restrict: "E",
      templateUrl:
        "./views/directives/bannerNewsletterError/bannerNewsletterError.component.html",
      controller: bannerNewsletterErrorController,
      controllerAs: "vm",
      scope: {
        site: "="
      },
      replace: true,
      bindToController: true
    };
  }

  function bannerNewsletterErrorController(
    $rootScope,
    $scope,
    $mdDialog,
    $state
  ) {
    var vm = this;
    vm.errorInstances = [];

    vm.openEmailCollectionDialog = function(instance) {
      var newScope = $scope.$new(true);
      newScope.instance = instance;
      newScope.site = vm.site;

      var options = {
        templateUrl: "./views/dialogs/emailCollection/emailCollection.html",
        scope: newScope,
        controller: "EmailCollectionDialogController",
        controllerAs: "vm"
      };

      $mdDialog.show(options).then(function(newTarget) {
        $state.reload();
      });

      return false;
    };

    $rootScope.$watch("instances", function(instances) {
      refresh(instances);
    });

    $rootScope.$on("$stateChangeSuccess", function() {
      refresh();
    });

    function refresh(instances) {
      buildErrorList(instances);
    }

    function buildErrorList(instances) {
      var errors = [];

      angular.forEach(instances, function(instance) {
        if (instance.mail_sync) {
          if (
            instance.mail_sync.sync_enabled &&
            instance.mail_sync.sync_error === 1
          ) {
            vm.providerName = getProviderName(
              instance.mail_sync.provider_type_id
            );
            errors.push(instance);
          }
        }
      });

      vm.errorInstances = errors;
    }

    function getProviderName(providerId) {
      switch (providerId) {
        case 1:
          return "Mailchimp";
        case 2:
          return "Conversio";
        case 3:
          return "Klaviyo";
      }

      return null;
    }

    refresh();
  }

  bannerNewsletterErrorController.$inject = [
    "$rootScope",
    "$scope",
    "$mdDialog",
    "$state"
  ];
})();
