/*global moment */

(function() {
  'use strict';

  angular.module('app.controllers').controller('DisplayOptionsDialogController', DisplayOptionsDialog);

  function DisplayOptionsDialog(DialogService, $scope) {
    var vm = this;
    vm.displayOptions = angular.copy($scope.displayOptions);
    vm.selectedSchedule = null;

    vm.saveButtonDisabled = function() {
      if (vm.displayConditionsForm.$invalid) return true;
      return angular.equals(vm.displayOptions, $scope.displayOptions);
    };

    vm.abort = function() {
      DialogService.hide(null);
    };

    vm.save = function() {
      DialogService.hide(vm.displayOptions);
    };

    vm.stopProp = function(e) {
      e.stopPropagation();
    };

    function updateSelectedSchedule(selectedKey) {
      angular.forEach(vm.displayOptions.scheduling, function(val, key) {
        if (vm.displayOptions.scheduling[key]) {
          vm.displayOptions.scheduling[key].enabled = key === selectedKey;
        }
      });
    }

    function initializeSelectedSchedule() {
      if (vm.displayOptions) {
        angular.forEach(vm.displayOptions.scheduling, function(val, key) {
          if (vm.displayOptions.scheduling[key]) {
            if (val.enabled) {
              vm.selectedSchedule = key;
            }
          }
        });
      }
    }

    (function initialize() {
      initializeSelectedSchedule();
      $scope.$watch('vm.selectedSchedule', function(selectedScheduleKey) {
        updateSelectedSchedule(selectedScheduleKey);
      }, true);
    }());
  }

})();
